ARG DOCKER_VERSION=latest
FROM docker:${DOCKER_VERSION}

RUN apk upgrade && apk add bash curl
